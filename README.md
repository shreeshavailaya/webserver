# Webserver

This is the simple filebased webserver implementation based on HTTP/1.1.

Below is the webserver configurations.

public static final int SERVER_PORT = 8081;

/* All the file resources needs to be placed under this folder */

SERVER_DOCUMENT_ROOT = "root/";

/* maximumPoolSize for ThreadPoolExecutor.
 * If the internal queue of tasks is full,
 *  and corePoolSize threads or more are running,
 *  but less than maximumPoolSize threads are running,
 *  then a new thread is created to execute the task.  
 * */

MAXIMUM_POOL_SIZE = 20;

/* corePoolSize for ThreadPoolExecutor.
 * If less than corePoolSize threads are created in the the thread pool,
 * when a task is delegated to the thread pool, 
 * then a new thread is created,
 *  even if idle threads exist in the pool.
 * */

CORE_POOL_SIZE = 10;

PROJECT IMPLEMENTATION DETAILS:

1. METHODs:
   Currently GET and HEAD is handled as part of this project.
   
2. File rendering works fine for html, xml, txt files.

3. JPG/PDF files are not rendered properly. (Need to work on this issue)

HTTP REQUEST

Request-Line   = Method SP Request-URI SP HTTP-Version CRLF
            	 Reference: https://www.w3.org/Protocols/rfc2616/rfc2616-sec5.html
                 
                 currently GET and HEAD methods are supported.

HTTP RESPONSE

Status-Line = HTTP-Version SP Status-Code SP Reason-Phrase CRLF

The first line of a Response message is the Status-Line, consisting of the protocol version followed by a numeric status code
and its associated textual phrase, with each element separated by SP characters. No CR or LF is allowed except in the final CRLF sequence.
	 

                  Reference: https://www.w3.org/Protocols/rfc2616/rfc2616-sec6.html
                  
After receiving and interpreting a request message, a server responds with an HTTP response message.

       Response      = Status-Line               ; 
                       *(( general-header        ; 
                        | response-header        ; 
                        | entity-header ) CRLF)  ; 
                       CRLF
                       [ message-body ]          ; 

JUNIT tests are integrated.





