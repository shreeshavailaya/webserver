package adobe.assignment.config;

public class ServerConfigurations {

	/* Server starts on this port */
	public static final int SERVER_PORT = 8081;

	/* All the file resources needs to be placed under this folder */
	public static final String SERVER_DOCUMENT_ROOT = "root/";

	/* maximumPoolSize for ThreadPoolExecutor.
	 * If the internal queue of tasks is full,
	 *  and corePoolSize threads or more are running,
	 *  but less than maximumPoolSize threads are running,
	 *  then a new thread is created to execute the task.  
	 * */
	public static final int MAXIMUM_POOL_SIZE = 20;

	/* corePoolSize for ThreadPoolExecutor.
	 * If less than corePoolSize threads are created in the the thread pool,
	 * when a task is delegated to the thread pool, 
	 * then a new thread is created,
	 *  even if idle threads exist in the pool.
	 * */
	public static final int CORE_POOL_SIZE = 10;

	/* ThreadPoolExecutor */
	public static final long KEEP_ALIVE_TIMER_IN_MILLI_SECONDS = 5000;


}
