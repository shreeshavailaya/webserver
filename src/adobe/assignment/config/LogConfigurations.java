package adobe.assignment.config;

public class LogConfigurations {

	public static enum logLevel {
		DEBUG,
		INFO,
		ERROR
	}

	public static final logLevel currentLogLevel = logLevel.DEBUG;

}
