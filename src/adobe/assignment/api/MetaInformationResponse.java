package adobe.assignment.api;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import adobe.assignment.util.Logger;
/*
 * The HEAD method is identical to GET except that the server MUST NOT return a message-body in the response.
 * The metainformation contained in the HTTP headers in response to a HEAD request SHOULD be identical
 * to the information sent in response to a GET request.
 * This method can be used for obtaining metainformation about the entity implied
 * by the request without transferring the entity-body itself.
 * This method is often used for testing hypertext links for validity, accessibility, and
 * recent modification.
 */
public class MetaInformationResponse extends HttpResponse {

	public MetaInformationResponse(int statusCode, String version, String reasonPhrase) {
		super(statusCode, version, reasonPhrase);
		getStatusLine();
		Logger.i("MetaInformationResponse constructed: <statusCode,version,reasonPhrase"+
				statusCode+","+version+","+reasonPhrase);
	}

	@Override
	public void write(OutputStream out) {
		Logger.i("write, Entered");

		try {
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
			writer.write(getStatusLine());
			writer.write("\r\n"); //CRLF
			/* no intermediate headers */
			writer.write("\r\n");
			writer.flush();
		} catch (IOException e) {
			Logger.e(e.getMessage());
		}

		Logger.i("write, Exited");

	}

}
