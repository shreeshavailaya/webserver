package adobe.assignment.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import adobe.assignment.util.Logger;

public class HttpRequest {

	/*
	 *  Request-Line   = Method SP Request-URI SP HTTP-Version CRLF
	 *  Reference: https://www.w3.org/Protocols/rfc2616/rfc2616-sec5.html
	 */

	private String version;

	/*
	 * Only GET/HEAD is supported as of now.
	 * Only these two are mandatory
	 */
	private methodType method;
	private String requestURI;
	
	
	
	public static enum methodType { GET, HEAD, UNSUPPORTED };

	public void parseRequestLine(InputStream in)
	{

		BufferedReader reader = new BufferedReader(new InputStreamReader(in));

		String line = "";
		try {
			line = reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String[] firstline = line.split(" ", 3);

		Logger.d("<METHOD,URI,VERSION> = "+firstline[0]+","+firstline[1]+","+firstline[2]);

		setMethod(firstline[0]);
		setRequestURI(firstline[1]);
		setVersion(firstline[2]);
		
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public methodType getMethod() {
		return method;
	}

	public void setMethod(String method) {

		this.method = methodType.UNSUPPORTED;
		
		if (method.equalsIgnoreCase("GET"))
		{
			this.method = methodType.GET;
		}
		else if (method.equalsIgnoreCase("HEAD"))
		{
			this.method = methodType.HEAD;
		}
	}

	public String getRequestURI() {
		return requestURI;
	}

	public void setRequestURI(String requestURI) {
		this.requestURI = requestURI;
	}


}
