package adobe.assignment.api;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Set;

import adobe.assignment.util.Logger;

public abstract class HttpResponse {

	private int statusCode;
	private String version;
	private String reasonPhrase;
	private HashMap<String, String> headers;

	public HttpResponse(int statusCode, String version, String reasonPhrase)
	{
		this.statusCode = statusCode;
		this.version = version;
		this.reasonPhrase = reasonPhrase;
		this.headers = new HashMap<>();
	}
	
	abstract public void write(OutputStream out);

	public String getStatusLine()
	{
		String statusLine = getVersion()+" "+getStatusCode()+" "+reasonPhrase;
		Logger.i("Status Line : "+statusLine);
		return statusLine;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getReasonPhrase() {
		return reasonPhrase;
	}

	public void setReasonPhrase(String reasonPhrase) {
		this.reasonPhrase = reasonPhrase;
	}
	
	public void addHeader(String key, String value)
	{
		headers.put(key, value);
	}
	
	public String getHeader(String key)
	{
		return headers.get(key);
	}
	
	public Set<String> getHeaders()
	{
		return headers.keySet();
	}
}
