package adobe.assignment.api;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import adobe.assignment.util.Logger;

public class FileResponse extends HttpResponse {

	private File requestedFile;

	public FileResponse(int statusCode, String version, String reasonPhrase, File requestedFile) {
		super(statusCode, version, reasonPhrase);
		this.requestedFile = requestedFile;

		try {
			setContentType();
			setContentLength();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void write(OutputStream out) {

		Logger.i("write, Entered");
		String CR_LF = "\r\n";

		try {
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
			writer.write(getStatusLine());
			writer.write(CR_LF); 

			for (String key: getHeaders()) {
				writer.write(key + ":" + getHeader(key));
				Logger.i("write Headers "+key+":"+getHeader(key));
				writer.write(CR_LF);
			}
			
			writer.write(CR_LF);

			if (requestedFile != null) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(requestedFile)));
				char[] buffer = new char[1024];
				int read;
				while ((read = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, read);
				}
				reader.close();
			}

			writer.flush();
		} catch (IOException e) {
			Logger.e(e.getMessage());
		}

		Logger.i("write, Exited");
	}

	private void setContentType() throws IOException {
		Path source = Paths.get(this.requestedFile.toURI());
		String contentType = Files.probeContentType(source);
		if (contentType != null) {
			addHeader("Content-Type", contentType);
		}
	}

	private void setContentLength() throws IOException {
		addHeader("Content-Length", requestedFile.length()+"");
	}

}
