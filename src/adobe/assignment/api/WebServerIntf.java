package adobe.assignment.api;

public interface WebServerIntf {
	
	HttpResponse serveRequests(HttpRequest request);

}
