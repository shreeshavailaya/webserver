package adobe.assignment.util;

import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import adobe.assignment.config.LogConfigurations;
import adobe.assignment.config.LogConfigurations.logLevel;

public class Logger {

	public static void e(String msg)
	{
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
		System.out.println("["+timeStamp+"]"+"ERROR:"+msg);
	}

	public static void d(String msg)
	{
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
		if (LogConfigurations.currentLogLevel == LogConfigurations.logLevel.DEBUG)
			System.out.println("["+timeStamp+"]"+"DEBUG:"+msg);
	}

	public static void i(String msg)
	{
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
		if (LogConfigurations.currentLogLevel == LogConfigurations.logLevel.DEBUG ||
				LogConfigurations.currentLogLevel == LogConfigurations.logLevel.INFO)
			System.out.println("["+timeStamp+"]"+"INFO:"+msg);
	}



}
