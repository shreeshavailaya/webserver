package adobe.assignment.test;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import adobe.assignment.config.ServerConfigurations;
import adobe.assignment.webserver.Webserver;

class WebserverTest {

	private static Thread serverThread;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {

		serverThread = new Thread(new Webserver());
		serverThread.start();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		serverThread.interrupt();
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test() {

		final WebClient webClient = new WebClient();

		webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);

		try {
			Page unexpectedResult403 = webClient.getPage("http://localhost:"+ServerConfigurations.SERVER_PORT+"/dummyfolder");
			Assert.assertEquals(403, unexpectedResult403.getWebResponse().getStatusCode());

			Page unexpectedResult404 = webClient.getPage("http://localhost:"+ServerConfigurations.SERVER_PORT+"/adobe2.html");
			Assert.assertEquals(404, unexpectedResult404.getWebResponse().getStatusCode());

			Page successfulResult200 = webClient.getPage("http://localhost:"+ServerConfigurations.SERVER_PORT+"/adobe.html");
			Assert.assertEquals(200, successfulResult200.getWebResponse().getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
