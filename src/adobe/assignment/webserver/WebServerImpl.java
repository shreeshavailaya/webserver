package adobe.assignment.webserver;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import adobe.assignment.api.FileResponse;
import adobe.assignment.api.HttpRequest;
import adobe.assignment.api.HttpResponse;
import adobe.assignment.api.MetaInformationResponse;
import adobe.assignment.api.WebServerIntf;
import adobe.assignment.config.ServerConfigurations;
import adobe.assignment.util.Logger;

public class WebServerImpl implements WebServerIntf {

	@Override
	public HttpResponse serveRequests(HttpRequest request) {

		HttpResponse response = null;
		String path = request.getRequestURI();
		try {
			path = URLDecoder.decode(request.getRequestURI(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Path requestedFile = Paths.get(ServerConfigurations.SERVER_DOCUMENT_ROOT, path);

		Logger.i("Requested File "+requestedFile.toString()); 
		switch (request.getMethod()) {
		case GET:
			if (Files.exists(requestedFile)) {
				if (Files.isDirectory(requestedFile)) {
					response = new MetaInformationResponse(403,"HTTP/1.1", "File not found");
				} else {
					response = new FileResponse(200,"HTTP/1.1", "File found",
							new File(Paths.get(ServerConfigurations.SERVER_DOCUMENT_ROOT, path).toString()));
				}
			}
			else
				response = new MetaInformationResponse(404,"HTTP/1.1", "File not found");
			break;
		case HEAD:
			if (Files.exists(requestedFile)) {
				if (Files.isDirectory(requestedFile)) {
					response = new MetaInformationResponse(403,"HTTP/1.1", "Access denied");
				} else {
					response = new MetaInformationResponse(200,"HTTP/1.1", "File found");
				}
			}
			else
				response = new MetaInformationResponse(404,"HTTP/1.1", "File not found");

			break;
		default:
			response = new MetaInformationResponse(403,"HTTP/1.1", "Method not supported");
			break;
		}

		return response;
	}
}


