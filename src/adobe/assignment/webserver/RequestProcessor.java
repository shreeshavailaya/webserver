package adobe.assignment.webserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import adobe.assignment.api.HttpRequest;
import adobe.assignment.api.HttpResponse;
import adobe.assignment.util.Logger;

public class RequestProcessor implements Runnable {

	Socket socket;
	private InputStream in;
	private OutputStream out;

	public RequestProcessor(Socket socket) {

		this.socket = socket;
		Logger.i("RequestProcessor Instantiated: ThreadID ="+Thread.currentThread().getId());

	}

	@Override
	public void run() {        

		try {
			in = socket.getInputStream();
			out = socket.getOutputStream();

			HttpRequest httpRequest = new HttpRequest();
			httpRequest.parseRequestLine(in);

			WebServerImpl serveRequest = new WebServerImpl();

			HttpResponse res = serveRequest.serveRequests(httpRequest);
			res.write(out);

		}
		catch (Exception e) {
			Logger.e(e.getMessage());
		}
		finally {
			try {
				in.close();
			} catch (IOException e) {
				Logger.e("Error while closing input stream");
			}
			try {
				out.close();
			} catch (IOException e) {
				Logger.e("Error while closing output stream");
			}
			try {
				socket.close();
			} catch (IOException e) {
				Logger.e("Error while closing client socket.");
			}
		}

	}
}
