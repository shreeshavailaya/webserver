package adobe.assignment.webserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import adobe.assignment.config.ServerConfigurations;
import adobe.assignment.util.Logger;

public class Webserver implements Runnable {

	private LinkedBlockingQueue<Runnable> connections;
	private ThreadPoolExecutor tPoolExecutor;
	private ServerSocket socket;

	public Webserver()
	{
		Logger.i("Webserver running at port number: "+ServerConfigurations.SERVER_PORT+"\n"+"Sample Usage: "+"http://localhost:8081/adobe.html");
	}

	@Override
	public void run() {

		try {
			socket = new ServerSocket(ServerConfigurations.SERVER_PORT);
			connections = new LinkedBlockingQueue<>();

			tPoolExecutor = new ThreadPoolExecutor(ServerConfigurations.CORE_POOL_SIZE,
					ServerConfigurations.MAXIMUM_POOL_SIZE,
					ServerConfigurations.KEEP_ALIVE_TIMER_IN_MILLI_SECONDS,
					TimeUnit.MILLISECONDS,
					connections);
		} catch (IOException e) {
			Logger.e("Exiting webserver : "+e.getMessage());
		}

		while (true)
		{
			try {
				tPoolExecutor.execute(new RequestProcessor(socket.accept()));
			} catch (IOException e) {
				Logger.e(e.getMessage());
			}
		}
	}


}
